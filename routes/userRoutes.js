const express = require("express");
const router = express.Router();
//contollers
const userController = require("./../controllers/userControllers");
//auth
const auth = require('./../auth');


//check if email exists
router.post('/checkEmail', (req, res) => {
	// console.log(req.body)
	userController.checkEmailExists(req.body).then(result => res.send(result));
});


//user registration
router.post('/register', (req, res)=> {
	userController.register(req.body).then(result => res.send(result));
})


//logging-in
router.post('/login', (req, res) => {

	userController.login(req.body).then( result => res.send(result))
})


//retrieve all users
router.get('/allUsers', auth.verify, (req, res) => {

	userController.getAllUsers().then( result => res.send(result))
})


//put to admin
router.put('/:userId/putAdmin', (req, res) => {
	//console.log(req.params.userId)

	userController.putToAdmin(req.params.userId).then( result => res.send(result))
})


//user set to admin
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
	//console.log(req.headers.authorization)
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false)
	} else {
		userController.userToAdmin(req.params.userId).then( result => res.send(result))
	}	
})


//retrieve all admin
router.get('/admin', auth.verify, (req, res) => {
	// console.log(userData)
	
	userController.getAllAdmin().then( result => res.send(result));
})


//create order
router.post('/checkout', auth.verify, (req, res) => {
	
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	} else {
		const userId = auth.decode(req.headers.authorization).id;
		//console.log(req.body)
		userController.checkoutOrder(userId, req.body).then(result => res.send(result));
	}
})


//retrieve all orders(admin only)
router.get('/orders', auth.verify, (req, res) => {
	// console.log(userData)

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false) {
		res.send(false);
	} else {
		userController.retrieveAllOrders().then(result => res.send(result));
	}
})


//retrieve user order details
router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);	
	
	if(userData.isAdmin === true) {
		res.send(false);
	} else {
		userController.retrieveMyOrders({userId : userData.id}).then(result => res.send(result));
	}
})







module.exports = router;

