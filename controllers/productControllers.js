const Product = require('./../models/Products')


//add product
module.exports.addProduct = (reqBody) => {
	 //console.log(reqBody)

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	})

	return newProduct.save().then( (product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//retrieve all active products
module.exports.allActiveProducts = () => {
	
	return Product.find({isActive: true}).then( result => {
		return result
	})
}


//retrieve single product
module.exports.singleProduct = (productId) => {
	//console.log(productId)

	return Product.findById(productId).then( result => {
		return result
	})
}


//retrieve all products
module.exports.allProducts = () => {
	
	return Product.find().then( result => {
		return result
	})
}


//update product
module.exports.updateProduct = (productId, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	}
	
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}


//archieve product
module.exports.archiveProduct = (productId) => {

	let updatedActiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(productId, updatedActiveProduct, {new: true})
	.then( (product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//unarchieve product
module.exports.unarchiveProduct = (productId) => {

	let updatedActiveProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(productId, updatedActiveProduct, {new: true})
	.then( (product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//delete a product
module.exports.deleteProduct = (params) => {

	return Product.findByIdAndDelete(params).then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


