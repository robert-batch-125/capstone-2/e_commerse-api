
const User = require("./../models/User");
const Product = require('./../models/Products');
const bcrypt = require('bcrypt');
const auth = require('./../auth');
const productController = require('./../controllers/productControllers');


//check email exists
module.exports.checkEmailExists = (reqBody) => {
	//console.log(reqBody)
	
	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){
			return true
		} else {
			return false
		}
	})
}


//user registration
module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return true
		}
	})
} 


//logging in
module.exports.login = (reqBody) => { 
	//Model.method
	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) //boolean return

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}


//get all users
module.exports.getAllUsers = () => {
	
	return User.find().then( result => {
		return result
	})
}


//put to admin
module.exports.putToAdmin = (params) => {

	let updateUserToAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(params, updateUserToAdmin, {new:true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//user set as admin
module.exports.userToAdmin = (userId) => {

	return User.findById(userId).then(user => {
			if(user === null){
				return false;
			}else{
				user.isAdmin = true;
				return user.save().then((updatedUser, error) => {
					if(error){
						return false;
					} else {
						return true;
					}
				})
			}
		})
	}


//get all admin
module.exports.getAllAdmin = () => {
	
	return User.find({isAdmin: true}).then( result => {		
		return result
	})
}


//create order
module.exports.checkoutOrder = (userId, cart) => {
	//console.log(cart.products[0].productName);

	return User.findById(userId).then(user => {
		if(user === null){
			return false;
		} else {
			var totalAmount = 0;

			for (var i = cart.products.length - 1; i >= 0; i--) {
				totalAmount = totalAmount + cart.products[i].price * cart.products[i].quantity;
			}
			cart.totalAmount = totalAmount;
			user.orders.push(
				{
					products: cart.products,
					totalAmount: cart.totalAmount
				}
			);
			//console.log(user.orders);
			return user.save().then((updatedUser, error) => {
				if(error){
					return false;
				} else {
					const currentOrder = updatedUser.orders[updatedUser.orders.length-1];

					cart.products.forEach(product => {

					Product.findById(product.productId).then(foundProduct => {

						foundProduct.quantity = foundProduct.quantity - product.quantity;
						//console.log(foundProduct.quantity);
							
						Product.findByIdAndUpdate(foundProduct._id.toString(), foundProduct).then(update => {
						//console.log(update);
						});

					});

				})

					return true;
				}
			})
		}
	})
} 


//retrieve all orders(admin only)
module.exports.retrieveAllOrders = () => {
	
	return User.find({isAdmin: false}).then(users => {

		let allOrders = [];

		users.forEach(user => {
			
			allOrders.push({
				lastName: user.lastName,
				email: user.email,
				userId: user._id,
				orders: user.orders
			});
		})
		return allOrders;
	})
}


//retrieve user order details
module.exports.retrieveMyOrders = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = undefined;
		return result;
	})
}

